import java.util.Random;
public class Die
{
private int faceValue;
private Random randomNum = new Random();

public Die()
{
    this.faceValue = 1;
    this.randomNum = new Random ();
}

public int getFaceValue()
{
    return faceValue;
}

public void roll()
{
    this.faceValue = randomNum.nextInt(6)+1;
}

public String toString()
{
    return "Die "+"face value"+faceValue;
}
}